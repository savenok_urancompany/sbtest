//
//  FileTableViewCell.h
//  SBTestTask
//
//  Created by User on 4/18/16.
//  Copyright © 2016 User. All rights reserved.
//

#import <DAContextMenuTableViewController/DAContextMenuTableViewController.h>

@protocol FileTableViewCellProtocol;

@interface FileTableViewCell : DAContextMenuCell
@property (weak, nonatomic) id <FileTableViewCellProtocol> customDelegate;
@property (strong, nonatomic) IBOutlet UIView *leftAccessoryView;
@property (strong, nonatomic) IBOutlet UIImageView *imageAccessoryView;
@property (strong, nonatomic) IBOutlet UILabel *customTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *customDescriptionLabel;
@end

@protocol FileTableViewCellProtocol <NSObject>
- (void)starButtonPressedInCell:(FileTableViewCell *)cell;
- (void)infinityButtonPressedInCell:(FileTableViewCell *)cell;
- (void)trashButtonPressedInCell:(FileTableViewCell *)cell;
@end
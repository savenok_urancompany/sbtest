//
//  TableViewDataSource.m
//  SBTestTask
//
//  Created by User on 4/18/16.
//  Copyright © 2016 User. All rights reserved.
//

#import "TableViewDataSource.h"
#import "TableViewDataSourceInternal.h"

@interface TableViewDataSource ()
@property (strong, nonatomic) NSArray *entries;
@property (strong, nonatomic) NSArray *currendNonParsedObject;
@property (strong, nonatomic) TableViewDataSourceInternal *internalDataSource;
@property (strong, nonatomic) NSMutableArray *current;
@end

@implementation TableViewDataSource
- (void)loadEntries
{
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"data" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    NSArray *tempArray = (NSArray *)parsedObject;
    self.internalDataSource = [[TableViewDataSourceInternal alloc] init];
    self.internalDataSource.parent = nil;
    [self parseToCurrent:tempArray];
    
}


- (FileModel *)fileModelAtIndex:(NSInteger) index
{
    if (index >= 0 && index < self.current.count)
        return [self.current objectAtIndex:index];
    return nil;
}

- (void)parseToCurrent:(NSArray *)array
{
    NSMutableArray *current = [[NSMutableArray alloc] init];
    for (NSDictionary *dictionary in array)
    {
        FileModel *model = [[FileModel alloc] init];
        if ([dictionary objectForKey:@"filename"]) model.filename = [dictionary objectForKey:@"filename"];
        [current addObject:model];
    }
    self.internalDataSource.items = [NSArray arrayWithArray:current];
}

- (void)switchToSubDirectoryAtIndex:(NSInteger) index
{
    NSDictionary *dictionary = [self.current objectAtIndex:index];
    if ([dictionary objectForKey:@"subDir"])
    {
        TableViewDataSourceInternal *internal = [[TableViewDataSourceInternal alloc] init];
        internal.parent = self.internalDataSource;
        self.internalDataSource = internal;
        [self parseToCurrent:[dictionary objectForKey:@"subDir"]];
    }
}

- (void)returnToParentDirectory
{
    self.internalDataSource = self.internalDataSource.parent;
}

@end

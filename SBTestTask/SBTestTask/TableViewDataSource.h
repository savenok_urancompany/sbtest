//
//  TableViewDataSource.h
//  SBTestTask
//
//  Created by User on 4/18/16.
//  Copyright © 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FileModel.h"

@interface TableViewDataSource : NSObject <UITableViewDataSource>
- (void)loadEntries;
- (FileModel *)fileModelAtIndex:(NSInteger) index;
- (void)switchToSubDirectoryAtIndex:(NSInteger) index;
- (void)returnToParentDirectory;
@end

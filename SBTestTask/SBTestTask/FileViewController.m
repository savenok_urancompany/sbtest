//
//  FileViewController.m
//  SBTestTask
//
//  Created by User on 4/18/16.
//  Copyright © 2016 User. All rights reserved.
//

#import "FileViewController.h"
#import "FileTableViewCell.h"

@interface FileViewController ()
@property (assign, nonatomic) NSInteger rowsCount;
@end

@implementation FileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"File View";
    self.rowsCount = 20;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setRowsCount:(NSInteger)rowsCount
{
    if (rowsCount < 0) {
        _rowsCount = 0;
    } else {
        _rowsCount = rowsCount;
    }
}

#pragma mark * Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.rowsCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    DAContextMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected");
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95.;
}

#pragma mark * DAContextMenuCell data source

- (NSUInteger)numberOfButtonsInContextMenuCell:(DAContextMenuCell *)cell
{
    return 0;
}

- (UIButton *)contextMenuCell:(DAContextMenuCell *)cell buttonAtIndex:(NSUInteger)index
{
    FileTableViewCell *demoCell = [cell isKindOfClass:[FileTableViewCell class]] ? (FileTableViewCell *)cell : nil;
    switch (index) {
        default: return nil;
    }
}

- (DAContextMenuCellButtonVerticalAlignmentMode)contextMenuCell:(DAContextMenuCell *)cell alignmentForButtonAtIndex:(NSUInteger)index
{
    return DAContextMenuCellButtonVerticalAlignmentModeCenter;
}

#pragma mark * DAContextMenuCell delegate

- (void)contextMenuCell:(DAContextMenuCell *)cell buttonTappedAtIndex:(NSUInteger)index
{
}

#pragma mark Custom Cell Protocol
- (void)starButtonPressedInCell:(FileTableViewCell *)cell
{
    NSLog(@"%@", @"star Button pressed");
}
- (void)infinityButtonPressedInCell:(FileTableViewCell *)cell
{
    NSLog(@"%@", @"infinity Button pressed");
}
- (void)trashButtonPressedInCell:(FileTableViewCell *)cell
{
    NSLog(@"%@", @"trash Button pressed");
}


@end

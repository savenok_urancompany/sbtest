//
//  FileModel.h
//  SBTestTask
//
//  Created by User on 4/18/16.
//  Copyright © 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FileType) {
    FileTypeImage,
    FileTypePDF,
    FileTypeMP3
};


@interface FileModel : NSObject
@property (strong, nonatomic) NSString *filename;
@property (strong, nonatomic) NSString *isFolder;
@property (strong, nonatomic) NSDate *modDate;
@property FileType fileType;
@property BOOL isOrange;
@property BOOL isBlue;
@end

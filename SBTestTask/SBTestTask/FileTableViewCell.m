//
//  FileTableViewCell.m
//  SBTestTask
//
//  Created by User on 4/18/16.
//  Copyright © 2016 User. All rights reserved.
//

#import "FileTableViewCell.h"

@implementation FileTableViewCell
- (IBAction)starButtonPressed:(id)sender {
    [self.customDelegate starButtonPressedInCell:self];
}
- (IBAction)infinityButtonPressed:(id)sender {
    [self.customDelegate infinityButtonPressedInCell:self];
}
- (IBAction)trashButtonPressed:(id)sender {
    [self.customDelegate trashButtonPressedInCell:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

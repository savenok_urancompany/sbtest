//
//  FileViewController.h
//  SBTestTask
//
//  Created by User on 4/18/16.
//  Copyright © 2016 User. All rights reserved.
//

#import <DAContextMenuTableViewController/DAContextMenuTableViewController.h>
#import "FileTableViewCell.h"

@interface FileViewController : DAContextMenuTableViewController <FileTableViewCellProtocol>

@end

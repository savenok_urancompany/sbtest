//
//  TableViewDataSourceInternal.h
//  SBTestTask
//
//  Created by User on 4/18/16.
//  Copyright © 2016 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TableViewDataSourceInternal : NSObject
@property (strong, nonatomic) TableViewDataSourceInternal *parent;
@property (strong, nonatomic) NSArray *items;
@end
